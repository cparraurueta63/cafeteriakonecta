###  Sobre el sistema

El sistema de venta en laravel para cafetería con adminlte, auch y boostrap 

![](https://cdn.diegooo.com/media/20210525094423/1_rzXx1ZgX05VRQdKE5pITKA.png)

# Requisitos

1.	Instalar laravel 8
2.	Php ^8.0.2
3.	Mysql 
4.	Servidor local, sugiero laragon

# Instalar dependencias
Laravel utiliza `[composer]` para administrar sus dependencias. Entonces, antes de usar Laravel, asegúrese de tener Composer instalado en su computador.
    cd YourDirectoryName
    composer install
[composer]: https://getcomposer.org/ "Composer"
# Archivo de configuración
Cambie el nombre o copie `.env.examble ` al archivo `.env` luego el comando `php artisan key:generate ` para generar la clave de la aplicación y luego configura las credenciales de su base de datos en el archivo `.env`
# Base de datos
1.	Migrar tabla a la  base de datos `php artisan migrate`
2.	`php artisan db:seed,` esto inicializará la configuración y creará un usuario administrador [correo electrónico: admin@gmail.com - contraseña: admin123
# Instalar dependencias de node
1.	`npm install `para instalar dependencias de node
2.	`npm run dev` para el desarrollo o npm run build para la producción
# Ejecutar servidor
`1.	php artisan serve `
